# %{first_commit}

[![pipeline status](https://gitlab.com/kisphp/thumbnails/badges/%{source_branch}/pipeline.svg)](https://gitlab.com/kisphp/thumbnails/-/commits/%{source_branch})
[![coverage report](https://gitlab.com/kisphp/thumbnails/badges/%{source_branch}/coverage.svg)](https://gitlab.com/kisphp/thumbnails/-/commits/%{source_branch})

[![Latest Stable Version](https://poser.pugx.org/kisphp/thumbnails/v/stable)](https://packagist.org/packages/kisphp/thumbnails)
[![Total Downloads](https://poser.pugx.org/kisphp/thumbnails/downloads)](https://packagist.org/packages/kisphp/thumbnails)
[![License](https://poser.pugx.org/kisphp/thumbnails/license)](https://packagist.org/packages/kisphp/thumbnails)
[![Monthly Downloads](https://poser.pugx.org/kisphp/thumbnails/d/monthly)](https://packagist.org/packages/kisphp/thumbnails)
